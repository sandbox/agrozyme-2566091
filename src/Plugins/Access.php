<?php
namespace Drupal\views_php_extra\Plugins;

use Drupal\views_php_extra\Traits\Plugin;
use views_php_plugin_access;

class Access extends views_php_plugin_access {
  use Plugin;

  function access($account) {
    $function = $this->getFunctionName('access');

    return function_exists($function) ?
      views_php_extra_check_access($function, $this->view->name, $this->display->id, $account) :
      parent::access($account);
  }

  function get_access_callback() {
    $function = $this->getFunctionName('access');

    return function_exists($function) ?
      ['views_php_extra_check_access', [$function, $this->view->name, $this->display->id]] :
      parent::get_access_callback();
  }

  protected function getAllArgumentsList() {
    $items = ['access' => '$view_name, $display_id, $account'];
    return $items;
  }

}
