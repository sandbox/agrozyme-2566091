<?php

namespace Drupal\views_php_extra\Plugins;

use Drupal\views_php_extra\Traits\Plugin;
use views_php_plugin_cache;

class Cache extends views_php_plugin_cache {
  use Plugin;

  function cache_get($type) {
    $list = $this->$this->getAllArgumentsList();
    return isset($list[$type]) ? $this->doCacheGet($type) : parent::cache_get($type);
  }

  protected function doCacheGet($type) {
    $function = $this->getFunctionName($type);
    $cache = cache_get($this->{'get_' . $type . '_key'}(), $this->table);
    $fresh = (FALSE == empty($cache));

    if ($fresh && function_exists($function)) {
      ob_start();
      $fresh = $function($this->view, $this, $cache);
      ob_end_clean();
    }

    $field = 'php_cache_' . $type;
    $options = &$this->options;
    $results = $options[$field];
    $options[$field] = "return $fresh;";
    $fresh = parent::cache_get($type);
    $options[$field] = $results;
    return $fresh;
  }

  protected function getAllArgumentsList() {
    $items = [
      'results' => '$view, $plugin, $cache',
      'output' => '$view, $plugin, $cache',
    ];

    return $items;
  }

}
