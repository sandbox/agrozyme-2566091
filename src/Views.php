<?php

namespace Drupal\views_php_extra;

use Drupal\mixin\Traits\Hook;
use Drupal\views_php_extra\Handlers\Area;
use Drupal\views_php_extra\Handlers\Field;
use Drupal\views_php_extra\Handlers\Filter;
use Drupal\views_php_extra\Handlers\Sort;
use Drupal\views_php_extra\Plugins\Access;
use Drupal\views_php_extra\Plugins\Cache;

class Views {
  use Hook;

  static function hook_views_api() {
    return ['api' => 3];
  }

  static function hook_check_access($function, $view_name, $display_id, $account = NULL) {
    global $user;

    if (FALSE == isset($account)) {
      $account = $user;
    }

    $access = FALSE;

    ob_start();
    if (function_exists($function)) {
      $access = (bool)$function($view_name, $display_id, $account);
    }
    ob_end_clean();
    return $access;
  }

  static function hook_views_data_alter(&$data) {
    $items = &$data['views']['php'];
    $items['area']['handler'] = Area::className();
    $items['field']['handler'] = Field::className();
    $items['filter']['handler'] = Filter::className();
    $items['sort']['handler'] = Sort::className();
  }

  static function hook_views_plugins_alter(&$plugins) {
    $plugins['access']['php']['handler'] = Access::className();
    $plugins['cache']['php']['handler'] = Cache::className();
  }

  protected static function getHookMap() {
    $class = get_called_class();
    $module = static::getType()->getModule();
    $hooks = [];
    $items = ['views_api', 'check_access'];

    foreach ($items as $item) {
      $hooks[$class]['hook_' . $item] = $module . '_' . $item;
    }

    return $hooks;
  }

}
