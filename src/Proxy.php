<?php

namespace Drupal\views_php_extra;

use Drupal\mixin\Arrays;
use Drupal\mixin\Caller;
use Drupal\mixin\Parameter\ParserList;
use Drupal\mixin\Traits\Hook;
use Drupal\views_php_extra\Plugins\Access;
use Drupal\views_php_extra\Traits\Base;
use ReflectionMethod;

abstract class Proxy {
  use Hook;

  protected $static = [];
  protected $type;
  protected $name;
  protected $view;
  protected $handler;
  protected $row;
  protected $row1;
  protected $row2;
  protected $data;
  protected $value;
  protected $account;
  protected $results;

  function __construct(array &$list = []) {
    foreach (array_keys(get_object_vars($this)) as $index) {
      if (isset($list[$index])) {
        $this->$index = &$list[$index];
      }
    }
  }

  static function hook_area_output($view, $handler, $results) {
    $data = ['type' => 'output'] + compact('name', 'view', 'handler', 'results');
    return static::invoke($data);
  }

  protected static function invoke(array &$data) {
    $name = static::getMethodName($data['handler'], $data['type']);
    $data['name'] = $name;
    return Caller::invoke([static::create($data), $name]);
  }

  protected static function getMethodName($handler, $type) {
    /** @var Base $handler */
    $items = [$handler->extendType(), $type, $handler->getFunctionNameSuffix()];
    $name = implode('_', $items);
    return $name;
  }

  static function create(array &$data = []) {
    return new static($data);
  }

  static function createHooks() {
    $callback = function ($function, $callback) {
      list($class, $method) = $callback;
      $wrapper = ParserList::create($callback);
      $parameter = $wrapper->toParameterListString();
      $argument = $wrapper->toArgumentListString();
      Caller::createFunction($function, $parameter, "return {$class}::{$method}({$argument});");
    };

    Arrays::walk(static::filterHookMap(), $callback);
  }

  protected static function filterHookMap() {
    $data = [];
    $class = get_called_class();
    $map = static::getHookMap();

    if (FALSE == is_array($map)) {
      return $data;
    }

    foreach ($map as $function) {
      $items = explode('_', $function);
      list($extend, $type) = $items;
      $method = 'hook_' . $extend . '_' . $type;
      $callback = [$class, $method];

      if (function_exists($function) || (FALSE == is_callable($callback))) {
        continue;
      }

      if ((new ReflectionMethod($class, $method))->isStatic()) {
        $data['views_php_' . $function] = $callback;
      }
    }

    //$filter = function ($function, $callback) {
    //  if (function_exists($function) || (FALSE == is_array($callback)) || (FALSE == is_callable($callback))) {
    //    return FALSE;
    //  }
    //
    //  list($class, $method) = $callback;
    //  return (new ReflectionMethod($class, $method))->isStatic();
    //};
    //$data = Arrays::filter($map, $filter);

    return $data;
  }

  static function hook_field_setup($view, $handler, &$static) {
    $data = ['type' => 'setup', 'static' => &$static] + compact('view', 'handler');
    return static::invoke($data);
  }

  static function hook_field_value($view, $handler, &$static, $row) {
    $data = ['type' => 'value', 'static' => &$static] + compact('view', 'handler', 'row');
    return static::invoke($data);
  }

  /** @noinspection PhpTooManyParametersInspection */
  static function hook_field_output($view, $handler, &$static, $row, $data, $value) {
    $data = ['type' => 'output', 'static' => &$static] + compact('view', 'handler', 'row', 'data', 'value');
    return static::invoke($data);
  }

  static function hook_filter_setup($view, $handler, &$static) {
    $data = ['type' => 'setup', 'static' => &$static] + compact('view', 'handler');
    return static::invoke($data);
  }

  static function hook_filter_filter($view, $handler, &$static, $row, $data) {
    $data = ['type' => 'filter', 'static' => &$static] + compact('view', 'handler', 'row', 'data');
    return static::invoke($data);
  }

  static function hook_sort_setup($view, $handler, &$static) {
    $data = ['type' => 'setup', 'static' => &$static] + compact('view', 'handler');
    return static::invoke($data);
  }

  static function hook_sort_sort($view, $handler, &$static, $row1, $row2) {
    $data = ['type' => 'sort', 'static' => &$static] + compact('view', 'handler', 'row1', 'row2');
    return static::invoke($data);
  }

  static function hook_access_access($view_name, $display_id, $account) {
    /** @var Access $handler */
    $view = views_get_view($view_name);
    $handler = views_get_plugin('access', 'php');
    $handler->init($view, $view->display[$display_id]);
    $data = ['type' => 'access'] + compact('name', 'view', 'handler', 'account');
    return static::invoke($data);
  }

  static function hook_cache_results($view, $handler, $value) {
    $data = ['type' => 'results'] + compact('view', 'handler', 'value');
    return static::invoke($data);
  }

  static function hook_cache_output($view, $handler, $value) {
    $data = ['type' => 'output'] + compact('view', 'handler', 'value');
    return static::invoke($data);
  }

}
