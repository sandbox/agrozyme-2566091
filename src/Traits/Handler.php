<?php

namespace Drupal\views_php_extra\Traits;

trait Handler {
  use Base;

  function extendType() {
    $field = 'handler_type';
    return $this->$field;
  }

  function isOverride() {
    $view = $this->view;
    $display = $view->display[$view->current_display];
    $handler = $display->handler;

    if ($handler->is_default_display()) {
      return FALSE;
    }

    $defaults = $handler->options['defaults'];
    $type = $this->formatPlural();
    return empty($defaults[$type]);
  }

}
