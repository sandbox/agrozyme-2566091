<?php

namespace Drupal\views_php_extra\Traits;

trait Plugin {
  use  Base;

  function extendType() {
    $field = 'plugin_type';
    return $this->$field;
  }

  function getDefaultDisplayOptions() {
    $defaults = $this->display->display_options['defaults'];
    return $defaults;
  }

}
