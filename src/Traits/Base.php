<?php

namespace Drupal\views_php_extra\Traits;

use ReflectionFunction;

trait Base {
  static function className() {
    return get_called_class();
  }

  function options_form(&$form, &$form_state) {
    /** @noinspection PhpUndefinedClassInspection */
    parent::options_form($form, $form_state);

    foreach ($this->getAllArgumentsList() as $type => $list) {
      $this->optionsFormAlter($form, $type, $list);
    }

  }

  protected function getAllArgumentsList() {
    $items = [];
    return $items;
  }

  protected function optionsFormAlter(&$form, $type, $list) {
    $function = $this->getFunctionName($type);
    $field = 'php_' . $type;
    $item = &$form[$field];
    $code = t('<code>@function(@arguments)</code>', ['@function' => $function, '@arguments' => $list]);
    $arguments = ['!code' => $code];

    if (function_exists($function)) {
      $reflect = new ReflectionFunction($function);
      $arguments['@filename'] = $reflect->getFileName();
      $item[$field]['#access'] = $item[$field . '_variables']['#access'] = FALSE;
      $item[$field . '_message'] = [
        '#type' => 'item',
        '#markup' => t('<strong>This field is using !code.<br />It is defined in @filename.</strong>', $arguments),
      ];
    } else {
      $item[$field]['#description'] .= t('<p>Alternately, this code can be supplied by your own custom function named: !code.</p>',
        $arguments);
    }
  }

  function getFunctionName($type) {
    $items = [$this->getFunctionNamePrefix(), $type, $this->getFunctionNameSuffix()];
    $name = implode('_', $items);
    return $name;
  }

  function getFunctionNamePrefix() {
    return 'views_php_' . $this->extendType();
  }

  function extendType() {
    return '';
  }

  function getFunctionNameSuffix() {
    $view = $this->view;
    $display = $this->isOverride() ? $view->current_display : 'default';
    $items = [$view->name, $display];
    $identity = &$this->options['id'];

    if (isset($identity)) {
      $items[] = $identity;
    }

    $suffix = implode('_', $items);
    return $suffix;
  }

  function isOverride() {
    return FALSE;
    static $plural = [
      'style_option' => TRUE,
      'row_option' => TRUE,
      'relationship' => TRUE,
      'field' => TRUE,
      'argument' => TRUE,
      'filter_group' => TRUE,
      'filter' => TRUE,
    ];

    $defaults = $this->getDefaultDisplayOptions();
    $type = $this->extendType();

    if (isset($plural[$type])) {
      $type .= 's';
    }

    return empty($defaults[$type]);
  }

  function formatPlural() {
    $types = views_object_types();
    $type = $this->extendType();

    if (isset($types[$type]['plural'])) {
      $type = $types[$type]['plural'];
    }

    return $type;
  }

  function getDefaultDisplayOptions() {
    return [];
  }

}
