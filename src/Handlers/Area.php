<?php

namespace Drupal\views_php_extra\Handlers;

use Drupal\views_php_extra\Traits\Handler;
use views_php_handler_area;

class Area extends views_php_handler_area {
  use Handler;

  function render($empty = FALSE) {
    $function = $this->getFunctionName('output');
    if ((!$empty || !empty($this->options['empty'])) && function_exists($function)) {
      ob_start();
      $function($this->view, $this, $this->view->result);
      return ob_get_clean();
    } else {
      return parent::render($empty);
    }
  }

  protected function getAllArgumentsList() {
    $items = ['output' => '$view, $handler, $results'];
    return $items;
  }
}
