<?php

namespace Drupal\views_php_extra\Handlers;

use Drupal\views_php_extra\Traits\Handler;
use views_php_handler_sort;

class Sort extends views_php_handler_sort {
  use Handler;

  function php_post_execute() {
    $function = $this->getFunctionName('sort');
    $view = $this->view;

    if (function_exists($function) && $view->style_plugin->build_sort()) {
      $field = 'php_sort_function';
      $this->$field = $function;
      ob_start();
      usort($view->result, [$this, 'php_sort']);
      ob_end_clean();
    } else {
      parent::php_post_execute();
    }
  }

  function php_pre_execute() {
    $function = $this->getFunctionName('setup');

    if (function_exists($function)) {
      ob_start();
      $function($this->view, $this, $this->php_static_variable);
      ob_end_clean();
    } else {
      parent::php_pre_execute();
    }
  }

  protected function getAllArgumentsList() {
    $items = [
      'setup' => '$view, $handler, &$static',
      'sort' => '$view, $handler, &$static, $row1, $row2',
    ];

    return $items;
  }

}
