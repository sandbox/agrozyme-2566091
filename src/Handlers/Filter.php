<?php

namespace Drupal\views_php_extra\Handlers;

use Drupal\views_php_extra\Traits\Handler;
use stdClass;
use views_php_handler_filter;

class Filter extends views_php_handler_filter {
  use Handler;

  function php_pre_execute() {
    $function = $this->getFunctionName('setup');

    if (function_exists($function)) {
      ob_start();
      $function($this->view, $this, $this->php_static_variable);
      ob_end_clean();
    } else {
      parent::php_pre_execute();
    }
  }

  function php_pre_render() {
    $function = $this->getFunctionName('filter');

    if (function_exists($function)) {
      $this->doFilter($function);
    } else {
      parent::php_pre_render();
    }
  }

  protected function doFilter($function) {
    ob_start();
    $view = $this->view;
    $row = new stdClass;

    foreach ($view->result as $index => $data) {
      foreach ($view->field as $id => $field) {
        $row->$id = $field->theme($data);
      }

      if (FALSE == $function($view, $this, $this->php_static_variable, $row, $data)) {
        unset($view->result[$index]);
      }
    }

    ob_end_clean();
  }

  protected function getAllArgumentsList() {
    $items = [
      'setup' => '$view, $handler, &$static',
      'filter' => '$view, $handler, &$static, $row, $data',
    ];

    return $items;
  }

}
