<?php

namespace Drupal\views_php_extra\Handlers;

use Drupal\views_php_extra\Traits\Handler;
use stdClass;
use views_php_handler_field;

class Field extends views_php_handler_field {
  use Handler;

  function php_post_execute() {
    $function = $this->getFunctionName('value');

    if (function_exists($function)) {
      $value = $this->options['php_value'];
      $this->doValue($function);
      $this->options['php_value'] = '';
      parent::php_post_execute();
      $this->options['php_value'] = $value;
    } else {
      parent::php_post_execute();
    }

  }

  protected function doValue($function) {
    ob_start();
    $view = &$this->view;

    foreach ($view->result as &$item) {
      $row = new stdClass;

      foreach ($view->display_handler->get_handlers('field') as $field => $handler) {
        $aliases = &$handler->aliases;
        $field_alias =& $handler->field_alias;

        if ((empty($aliases) || empty($aliases['entity_type'])) && ($field_alias != $this->field_alias)) {
          $row->$field = isset($item->{$field_alias}) ? $item->{$field_alias} : NULL;
        }
      }

      $item->{$this->field_alias} = $function($this->view, $this, $this->php_static_variable, $row);
    }

    ob_end_clean();
  }

  function php_pre_execute() {
    $function = $this->getFunctionName('setup');

    if (function_exists($function)) {
      ob_start();
      $function($this->view, $this, $this->php_static_variable);
      ob_end_clean();
    } else {
      parent::php_pre_execute();
    }
  }

  function render($values) {
    $function = $this->getFunctionName('output');
    return function_exists($function) ? $this->doOutput($function, $values) : parent::render($values);
  }

  protected function doOutput($function, $data) {
    $view = $this->view;
    $field_alias = $this->field_alias;
    $rendered_fields = &$view->style_plugin->rendered_fields;
    $row = new stdClass;

    if (empty($rendered_fields)) {
      foreach ($view->field as $index => $item) {
        if ($item->field_alias != $field_alias) {
          $row->$index = $item->get_value($data);
        }
      }
    } else {
      foreach ($rendered_fields{$view->row_index} as $index => $item) {
        $row->$index = $item;
      }
    }

    $value = isset($data->{$field_alias}) ? $data->{$field_alias} : NULL;
    ob_start();
    $function($view, $this, $this->php_static_variable, $row, $data, $value);
    $result = ob_get_clean();
    return $result;
  }

  protected function getAllArgumentsList() {
    $items = [
      'setup' => '$view, $handler, &$static',
      'value' => '$view, $handler, &$static, $row',
      'output' => '$view, $handler, &$static, $row, $data, $value',
    ];

    return $items;
  }

}
